import time
import numpy
import math
import struct
import json
from bson.objectid import ObjectId
import hashlib
from landontoda.utils import primes
from landontoda.models import Plot, User, Mint, File, MongoDBLeaf
from landontoda.controllers.todaq_driver import TODAQDriver
from landontoda.services.blockchain_service import BlockchainInterface, BlockchainService, BlockError

FLOAT_PER = 0.000000000001

class LOTSpace:
    @classmethod
    def to_lot_space(cls, coordinate):
        lat, lon = coordinate['lat'], coordinate['lon']
        lat+=90
        lon+=180
        ls_lat = int((lat+FLOAT_PER)*10000000)#whole number
        ls_lon = int((lon+FLOAT_PER)*10000000)#whole number
        return {"ls_lat": ls_lat, "ls_lon": ls_lon}

    @classmethod
    def from_lot_space(cls, lot_coordinate):
        ls_lat, ls_lon = lot_coordinate['ls_lat'], lot_coordinate['ls_lon']
        lat = ls_lat/10000000.0
        lon = ls_lon/10000000.0
        lat-=90
        lon-=180
        return {"lat": lat, "lon": lon}

    @classmethod
    def lot_num_permute(cls, lot, total_lots, in_cs, out_cs, rounds):
        res = primes.PrimesCalc.full_permute(lot, total_lots, in_cs, out_cs, rounds)
        return res

    @classmethod
    def lot_num_invert(cls, lot, total_lots, in_cs, out_cs, rounds):
        res = primes.PrimesCalc.full_invert(lot, total_lots, in_cs, out_cs, rounds)
        return res


test_time = 0
test_time2 = 0


class PlotObjectService:
    @classmethod
    def generate_plot_type(cls, account, payload):
        file_type = "0"*64
        transaction_id = TODAQDriver.create_file(account, payload, file_type)
        print (transaction_id)

        transaction_id = transaction_id['data'][0]['id']
        return transaction_id

    @classmethod
    def get_file_id(cls, transaction_id):
        t = time.time()
        for i in range(12):
            file_id = TODAQDriver.get_transaction_files(transaction_id, 1, 100)
            print("+++++++++++")
            print(file_id)
            try:
                file_id = file_id['data'][0]['id']
                return file_id
            except:
                pass
            time.sleep(5)
        return None
        
    @classmethod
    def compute_plot_data_hash(cls, plot_data):
        in_string = json.dumps(plot_data, sort_keys=True)
        output = hashlib.sha256(in_string.encode("utf-8")).hexdigest()
        return output

    @classmethod
    def create_plot_object_presign(cls, coordinates, creator_type, creator_id, creator_key, 
                                        creator_name, owner_id, owner_key, owner_name, 
                                        plot_type, country, region, use, project, price_sqcm, 
                                        total_value, invest_by_owner, invest_returns, 
                                        description):
        max_depth = 32
        max_time = 30

        user = User.find_one({"_id": owner_id})
        
        obj = {
            "coordinates": coordinates,
            "country": country,
            "region": region,
            "use": use,
            "project": project,
            "price_sqcm": price_sqcm,
            "total_value": total_value, 
            "creator_id": creator_id,
            "creator_key": creator_key,
            "creator_name": creator_name,
            "creator_type": creator_type,
            "owner_id": owner_id,
            "owner_key": owner_key,
            "owner_name": owner_name,
            "plot_type": plot_type,
            "description": description,
            "invest_by_owner": invest_by_owner,
            "invest_returns": invest_returns,
            "pushed": False
        }
        plot_data = cls.create_plot(coordinates, max_time=max_time, max_depth=max_depth)

        obj['plot_data_hash'] = cls.compute_plot_data_hash(plot_data)
        obj['plot_hash'] = plot_data['squares_hash']

        obj['_id'] = obj['plot_hash']
        obj['plot_data'] = plot_data
        obj['total_lots'] = plot_data['total_lots']
        obj['price_sqcm'] = obj['total_value']/plot_data['total_lots']
        
        #Return the message to sign for these public keys:
        height, _, __ = BlockchainService.get_height()

        if creator_type == "country":
            #root key that assigns countries
            root_private_key = BlockchainService.get_private_key()
            root_public_key = root_private_key.verifying_key.to_string().hex()

            if root_public_key != creator_key:
                raise BlockError("Only root key may create country plots.")
        elif creator_type == "owner": 
            #We'll check the sig in the add_plot_to_blockchain step where we can
            #assess all the country signatures submitted before.
            pass

        message = plot_data['squares_hash']+"_"+owner_key+"_"+str(height+1)
        obj['sig_tuple'] = [message, owner_key, creator_key, None]

        #Save the plot now
        obj['saved'] = False
        obj['creation_time'] = ObjectId()
        Plot.save(obj)

        return_data = {"message": message, "height": height+1, "obj": obj}
        return return_data

    @classmethod
    def create_plot_object_postsign(cls, message, signature):
        squares_hash, owner_public, height = message.split("_")
        height, _, __ = BlockchainService.get_height()
        plot_obj = Plot.find_one({"_id": squares_hash})

        if not plot_obj or not plot_obj.get("sig_tuple"):
            raise BlockError("Invalid plot hash")
 
        #signature = root_private_key.sign(message.encode("utf-8")).hex()
        sig_tuple = [message, plot_obj['owner_key'], plot_obj['creator_key'], signature]
        try:
            BlockchainInterface.add_plot_def(plot_obj['plot_data'],
                                             plot_obj['owner_name'], 
                                             sig_tuple, 
                                             leaf_type=plot_obj['creator_type'])
        except:
            raise

        Plot.update({"_id": squares_hash}, {"$set": {"pushed": True}})
        return True

    @classmethod
    def split_lot_file(cls, account, file_id, number, recipient1, recipient2):
        file_object = TODAQDriver.get_file_by_id(account, file_id)
        full_object = File.find_one({"_id": file_object})
        #create split files...
        #{"originating_file": ...}
        #send original file to 00000, and add split files to the 
        #....

    @classmethod
    def create_plot(cls, coordinates_list, max_time=None, max_depth=None):
        #convert coordinates to lot space:
        lot_coordinates = [LOTSpace.to_lot_space(x) for x in coordinates_list]
        lot_coordinates = [(x['ls_lat'], x['ls_lon']) for x in lot_coordinates]

        start_square = {"tl": [0, 0], 
                        "br": [2**32, 2**32],
                        "w": 32}
        t_t = time.time()
        squares = cls.get_squares([start_square], lot_coordinates, max_time=max_time,
                                  max_depth=max_depth)
        print(len(squares))
        total_lots = sum([((2**x['w'])**2) for x in squares])
        print (total_lots)
        squares.sort(key=lambda x: (-x['w'], x['tl'][0], x['tl'][1]))
        sorted_json = json.dumps(squares, sort_keys=True)

        rounds = 4
        in_cs = list()
        out_cs = list()
        hash_number1 = hashlib.sha256(sorted_json.encode("utf-8")).hexdigest()
        hash_number = hash_number1
        for i in range(rounds):
            in_c = int(hash_number[:32],16)%total_lots
            out_c = int(hash_number[32:],16)%total_lots
            in_cs.append(in_c)
            out_cs.append(out_c)
            hash_number = hashlib.sha256(hash_number.encode("utf-8")).hexdigest()
           
        print(time.time()-t_t)
        output = {
                  "squares": squares,
                  "total_lots": total_lots,
                  "squares_hash": hash_number,
                  "in_cs": in_cs,
                  "out_cs": out_cs,
                  "rounds": rounds
                 }
        #return output

        start = 2**squares[0]['w']
        x = cls.lot_number_to_coordinates(start, squares, total_lots, in_cs, out_cs, rounds)
        coordinate = {"lat": (x['tl']['lat']+x['br']['lat'])/2,
                      "lon": (x['tl']['lon']+x['br']['lon'])/2}
        y = cls.coordinate_to_lot_number(coordinate, squares, total_lots, in_cs, out_cs, rounds)
        print (x,y)
        return output

    @classmethod
    def lot_number_to_coordinates(cls, lot, squares, total_lots, in_cs, out_cs, rounds):
        #first convert lot number to lot space number:
        lot_space_number = LOTSpace.lot_num_permute(lot, total_lots, in_cs, out_cs, rounds)
        print(['+11', lot_space_number])
        #lot_space_number = lot
        counter = 0
        this_entry = None
        for entry in squares:
            if counter+(2**entry['w'])**2 >= lot_space_number:
                remainder = lot_space_number-counter
                this_entry = entry
                break
            counter+=(2**entry['w'])**2
        entry = this_entry
        y = int(remainder/(2**entry['w']))
        x = remainder%(2**(entry['w']))

        ls_tl = [entry['tl'][0]+y, entry['tl'][1]+x]
        ls_br = [entry['tl'][0]+y+1, entry['tl'][1]+x+1]
        tl = LOTSpace.from_lot_space({"ls_lat": ls_tl[0], "ls_lon": ls_tl[1]})
        br = LOTSpace.from_lot_space({"ls_lat": ls_br[0], "ls_lon": ls_br[1]})
        return {"tl": tl, "br": br}

    @classmethod
    def coordinate_to_lot_number(cls, coordinate, squares, total_lots, in_cs, out_cs, rounds):
        ls_coordinate = LOTSpace.to_lot_space(coordinate)
        ls_lat = ls_coordinate['ls_lat']
        ls_lon = ls_coordinate['ls_lon']

        start_number = 0
        for square in squares:
            if square['tl'][0] <= ls_lat < square['br'][0] and\
                    square['tl'][1] <= ls_lon < square['br'][1]:
                #this square has the point in it, so compute the number
                #for this point:
                r_lat = ls_lat-square['tl'][0]
                r_lon = ls_lon-square['tl'][1]
                #print (ls_lon, square['tl'][1])
                x = r_lon
                y = r_lat
                lot_number = y*(2**square['w'])+x+start_number
                #print ("---", square['tl'], square['br'], '-', ls_lat, ls_lon)
                #print ( ['++', lot_number] )
                lot_number = LOTSpace.lot_num_invert(lot_number, total_lots, in_cs, out_cs, rounds)
                return lot_number
            else:
                start_number+=((2**square['w'])**2)
        else:
            return -1#not in this plot
        pass

    @classmethod
    def get_squares(cls, square_list, polygon, max_time=30, max_depth=None): 
        #get max/min values for polygon:
        t_time = time.time()
        max_min_lat = [max(x[0] for x in polygon), min(x[0] for x in polygon)]
        max_min_lon = [max(x[1] for x in polygon), min(x[1] for x in polygon)]
        max_min = [max_min_lat, max_min_lon]
        print(max_min)
        #do sub dividing algo with coordinates list.
        out_squares = list()
        new_quares = list()

        poly_segs = list()
        l = len(polygon)
        for i in range(len(polygon)):
            poly_segs.append([polygon[i], polygon[(i+1)%l]])

        if max_depth == None:
            max_depth = 33

        for i in range(33):
            new_squares = list()
            print(i, len(out_squares), len(square_list))
            for square in square_list:
                if cls.polygon_intersect_square(square, poly_segs):
                    new_squares.extend(cls.subdivide(square))
                elif cls.square_in_polygon(square, polygon):
                    out_squares.append(square)
                elif cls.polygon_in_square(square, polygon):
                    new_squares.extend(cls.subdivide(square))
                #elif cls.square_outside_polygon(square, polygon):
                #    pass
            if max_time and time.time()-t_time > max_time:
                break
            if out_squares and i >= max_depth:
                break
            square_list = new_squares
        print (test_time, test_time2)
        return out_squares

    @classmethod
    def square_outside_polygon_quick(cls, square, max_min):
        tl = square['tl']
        br = square['br']
        #check for fast exit with max_min:
        if max(tl[0], br[0]) < max_min[0][1] or\
           min(tl[0], br[0]) > max_min[0][0] or\
           max(tl[1], br[1]) < max_min[1][1] or\
           min(tl[1], br[1]) > max_min[1][0]:
            return True
        return False

    @classmethod
    def subdivide(cls, square):
        out_squares = list()
        tl, br = square['tl'], square['br']
        w = square['w']

        if w == 0:
            return list()

        mp = ((tl[0]+br[0])/2, (tl[1]+br[1])/2)
        tp = ((tl[0]+br[0])/2, tl[1])
        lp = (tl[0], (tl[1]+br[1])/2)
        rp = (br[0], (tl[1]+br[1])/2)
        bp = ((tl[0]+br[0])/2, br[1])

        ##topleft
        out_squares.append({"tl": tl, "br": mp, "w": w-1})
        ##topright
        out_squares.append({"tl": tp, "br": rp, "w": w-1})
        ##bottomleft
        out_squares.append({"tl": lp, "br": bp, "w": w-1})
        ##bottomright
        out_squares.append({"tl": mp, "br": br, "w": w-1})
        return out_squares

    @classmethod
    def polygon_intersect_square(cls, square, poly_segs):
        global test_time
        t = time.time()
        square_segs = list()
        tl = square['tl']
        br = square['br']
        tr = [br[0], tl[1]]
        bl = [tl[0], br[1]]

        #[lat[max,min], lon[max,min]]
        square_segs = [[tl, tr],
                       [tr, br],
                       [br, bl], 
                       [bl, tl]
                       ]
        for def_ in [square_segs, poly_segs]:
            for seg in def_:
                if len(seg) > 2:
                    break
                seg.append([ [max(seg[0][0], seg[1][0]), 
                              min(seg[0][0], seg[1][0])],
                             [max(seg[0][1], seg[1][1]), 
                              min(seg[0][1], seg[1][1])]])

        for poly_seg in poly_segs:
            for square_seg in square_segs:
                if poly_seg[2][0][0] < square_seg[2][0][1] or\
                   poly_seg[2][0][1] > square_seg[2][0][0] or\
                   poly_seg[2][1][0] < square_seg[2][1][1] or\
                   poly_seg[2][1][1] > square_seg[2][1][0]:
                    continue
                if cls.line_intersects_line(poly_seg, square_seg):
                    test_time+=(time.time()-t)
                    return True
        test_time+=(time.time()-t)
        return False

    @classmethod
    def square_in_polygon(cls, square, polygon):
        #algo: 
        # -check crossing number for each point in the square to the polygon
        # -if all numbers are odd, square is inside the poly
        tl = square['tl']
        br = square['br']
        tr = [br[0], tl[1]]
        bl = [tl[0], br[1]]

        square_points = [tl, br, tr, bl]
 
        for point in square_points:
            crossing_num = cls.crossing_number(point, polygon)
            if crossing_num %2 == 0:
                return False
        return True

    @classmethod
    def polygon_in_square(cls, square, polygon):
        #algo: 
        # -check crossing number for each point in the polygon to the square
        # -if all numbers are odd, then polygon is inside the square
        square_segs = list()
        tl = square['tl']
        br = square['br']
        tr = [br[0], tl[1]]
        bl = [tl[0], br[1]]

        square_segs = [[tl, tr],[tr, br], [br, bl], [bl, tl]]
        square_segs = [tl, tr, br, bl]
        #print ('compare')
        #print('sq',  square_segs)
        #print("pq", polygon)
       
        for point in polygon:
            crossing_num = cls.crossing_number(point, square_segs)
            #print(crossing_num)
            if crossing_num %2 == 0:
                return False
        return True


    @classmethod
    def square_outside_polygon(cls, square, polygon):
        #algo: 
        # -check crossing number for each point in the square to the polygon
        # -if all numbers are even, then square is outside the poly
        tl = square['tl']
        br = square['br']
        tr = [br[0], tl[1]]
        bl = [tl[0], br[1]]

        square_points = [tl, br, tr, bl]
 
        for point in square_points:
            crossing_number = cls.crossing_number(point, polygon)
            if crossing_number% 2 == 1:
                return False
        return True


    @classmethod
    def crossing_number(cls, point, poly):
        global test_time2
        crossing_num = 0
        t = time.time()

        for i in range(len(poly)):
            seg = [poly[i], poly[(i+1)%len(poly)]]
            line1 = [point, [2**33, point[1]]]
            line2 = seg
            #print("--------------")
            #print(line1, line2)
            if cls.line_intersects_line(line1, line2):
                crossing_num+=1
        test_time2+=(time.time()-t)
        return crossing_num

    @classmethod
    def line_intersects_line(cls, line1, line2):
        """
            line1 = [(x1,y1), (x2,y2)]
            line2 = [(w1,z1), (w2,z2)]

            x1+t(x2-x1) = w1+s(w2-w1)
            y1+t(y2-y1) = z1+s(z2-z1)

            system of equations:
            1.) t(x2-x1) - s(w2-w1) = w1-x1
            2.) t(y2-y1) - s(z2-z1) = z1-y1

            simplify:
            1.) t(a) - s(b) = e 
            2.) t(c) - s(d) = f
        """
        x1,y1 = line1[0]
        x2,y2 = line1[1]
        w1,z1 = line2[0]
        w2,z2 = line2[1]

        a = numpy.array([[x2-x1, -1*(w2-w1)], [y2-y1, -1*(z2-z1)]])
        b = numpy.array([w1-x1, z1-y1])
        try:
            s, t = numpy.linalg.solve(a,b)
        except:
            return False

        if -FLOAT_PER <= s <= 1+FLOAT_PER and -FLOAT_PER <= t <= 1+FLOAT_PER:
            return True
        return False

def main2():
    line1 = [(0,-3), (1, 0)]
    line2 = [(0, 4), (1, 6.3)]
    LOTService.line_intersects_line(line1, line2)

def main3():
    square = [(121962496, 54687744), [121964544, 54687744], (121964544, 54689792), [121962496, 54689792]]
    square = [LOTSpace.from_lot_space({"ls_lat": x[0], "ls_lon": x[1]}) for x in square]
    print(square)

def main():
    data_leb = """34.636976214776226, 35.97196372282143
34.621156074396204, 36.30155356657143
34.68441853154681, 36.30430014860268
34.68667701191888, 36.36197837125893
34.6460149404735, 36.37845786344643
34.63019652400213, 36.46634848844643
34.58498442786394, 36.45536216032143
34.52164614941736, 36.34549887907143
34.50127717924237, 36.34000571500893
34.50806738917334, 36.45810874235268
34.41295413285971, 36.57071860563393
34.351752706085186, 36.53775962125893
34.3018518151125, 36.60367759000893
34.22467391155121, 36.59269126188393
34.19968943194045, 36.61466391813393
34.04279977376877, 36.50480063688393
34.043179421293424, 36.42759170554867
33.90651786187459, 36.28202285789242
33.847229677987485, 36.41111221336117
33.81985201227522, 36.37540664695492
33.797030593743116, 36.07877578757992
33.63939703548642, 35.95792617820492
33.5705402361671, 36.054885605363225
33.52017927004216, 36.016433456925725
33.511019578067454, 35.945022324113225
33.45374953806975, 35.950515488175725
33.389562161947666, 35.826919296769475
33.3230323781772, 35.777480820206975
33.26334177119168, 35.626418808488225
33.2380757626768, 35.615432480363225
33.26334177119168, 35.563247421769475
33.090928756154675, 35.502822617081975
33.05640547284471, 35.376479843644475
33.095530836935524, 35.302322128800725
33.09322982665847, 35.17994795225834
33.08632643447548, 35.10853681944584
33.116237218874865, 35.10853681944584
33.28171246112653, 35.18544111632084
33.446874589264375, 35.25959883116459
33.78995331041056, 35.44361982725834
33.9040089443694, 35.44911299132084
33.94958854352186, 35.54877668471557
34.2361783575887, 35.63392072768432
34.36323976441801, 35.69434553237182
34.41083821204089, 35.78772932143432
34.46520328893445, 35.79596906752807
34.4742606956958, 35.89484602065307
34.530847212087075, 35.97999006362182"""

    data_leb_airport = """33.791387251269185, 35.48005750811178
33.79545303002833, 35.48216035997945
33.797307531649835, 35.48220327532369
33.804368535443764, 35.48773935473043
33.82718308518858, 35.504936890302275
33.828929949392595, 35.50399275272903
33.83130827193607, 35.50078636789407
33.83355412527054, 35.49576527261819
33.83191430166443, 35.494692389012236
33.832270787732995, 35.49413448953714
33.835470172908195, 35.4939064985373
33.83885658965379, 35.49386358319306
33.838571422891185, 35.48613882123017
33.8379297941963, 35.48485136090302
33.82918261215549, 35.48442756280333
33.82079008559955, 35.48690798123298
33.8191143631644, 35.48596384365974
33.8165828904144, 35.48343183834968
33.80538816700519, 35.481593120146144
33.79718603392184, 35.478331553984034
33.79586648694145, 35.478331553984034"""
 
    data_leb_plot2 = """33.97466255285222, 35.6133539253536
33.97288308214406, 35.61713047564657
33.969324029008085, 35.61979122698934
33.96804273341496, 35.618503766662194
33.967046156829866, 35.617387967712
33.96562245573706, 35.616701322204186
33.96334448441969, 35.61755962908895
33.96270379399477, 35.616443830138756"""

    data_leb_airport_intersect = """33.82902887764962, 35.49123293406865
33.83573079320789, 35.51389223582646
33.814767734714266, 35.52110201365849
33.80991840438161, 35.49809938914677"""

    polygon_leb = [{"lat": float(x[0]), "lon": float(x[1])} for x in [y.split(",") for y in data_leb.split("\n")]]
    polygon_leb_airport = [{"lat": float(x[0]), "lon": float(x[1])} for x in [y.split(",") for y in data_leb_airport.split("\n")]]
    polygon_leb_plot2 = [{"lat": float(x[0]), "lon": float(x[1])} for x in [y.split(",") for y in data_leb_plot2.split("\n")]]
    polygon_leb_airport_intersect = [{"lat": float(x[0]), "lon": float(x[1])} for x in [y.split(",") for y in data_leb_airport_intersect.split("\n")]]

    #polygon = [
    #    {"lat": 37.900974, "lon": -122.646057},
    #    {"lat": 37.900720, "lon": -122.645526},
    #    {"lat": 37.899653, "lon": -122.646347},
    #    {"lat": 37.899839, "lon": -122.646926}
    #]
    import time
    from landontoda import config
    from ecdsa import SigningKey, SECP256k1
    config.load_config()
    BlockchainService.wipe_data()

    md = 15
    ##########################################
    #create country
    t_time = time.time()
    plot_def = PlotObjectService.create_plot(polygon_leb, max_time=30, max_depth=md)
    s_time = time.time()-t_time

    t_time = time.time()
    
    #assigning plot now
    height, _, __ = BlockchainService.get_height()
    #root key that assigns countries
    root_private_key = BlockchainService.get_private_key()
    root_public_key = root_private_key.verifying_key.to_string().hex()
    #lebanon key for assigning owners inside lebanon
    lebanon_key_hex = "23e7473ce74f18fb4cb5694169369a0e49dd45d974506f183afcf98018243e3a"
    lebanon_key_private = SigningKey.from_string(bytes.fromhex(lebanon_key_hex), curve=SECP256k1)
    lebanon_public = lebanon_key_private.verifying_key.to_string().hex()
    

    message = plot_def['squares_hash']+"_"+lebanon_public+"_"+str(height+1)
    signature = root_private_key.sign(message.encode("utf-8")).hex()
    sig_tuple = [message, lebanon_public, root_public_key, signature]
    BlockchainService.add_plot_to_tree(plot_def, "lebanon", sig_tuple, leaf_type="country")
    print(333)
    t_time = time.time()-t_time

    u_time = time.time()
    BlockchainService.compute_block_tree()
    BlockchainService.submit_block_tree(height)    
    u_time = time.time()-u_time


    #####################
    # Assign land plot (airport)
    height, _, __ = BlockchainService.get_height()
    new_owner_hex = "f473ceee7f4e1a07e48f2f50a7884dff574a0359a8f2f1cf30e06c6dd2dcc70f"
    new_owner_private = SigningKey.from_string(bytes.fromhex(new_owner_hex), curve=SECP256k1)
    new_owner_public = new_owner_private.verifying_key
    new_owner = new_owner_public.to_string().hex()

    plot_def_airport = PlotObjectService.create_plot(polygon_leb_airport, max_time=30, max_depth=md)
    message = plot_def_airport['squares_hash']+"_"+new_owner+"_"+str(height)
    signature = lebanon_key_private.sign(message.encode("utf-8")).hex()
    sig_tuple = [message, new_owner, lebanon_public, signature]

    BlockchainService.add_plot_to_tree(plot_def_airport, new_owner, sig_tuple, leaf_type="owner") 
    print(222)
    plot_def_plot2 = PlotObjectService.create_plot(polygon_leb_plot2, max_time=30, max_depth=md)
    message = plot_def_plot2['squares_hash']+"_"+new_owner+"_"+str(height)
    signature = lebanon_key_private.sign(message.encode("utf-8")).hex()
    sig_tuple = [message, new_owner, lebanon_public, signature]

    BlockchainService.add_plot_to_tree(plot_def_plot2, new_owner, sig_tuple, leaf_type="owner") 
    print(444)
    u_time = time.time()
    BlockchainService.compute_block_tree(height=height+1)
    BlockchainService.submit_block_tree(height+1)
    u_time = time.time()-u_time

    #####################
    # Assign land plot intersect (airport)
    height, _, __ = BlockchainService.get_height()
    new_owner_hex = "f473ceee7f4e1a07e48f2f50a7884dff574a0359a8f2f1cf30e06c6dd2dcc70f"
    new_owner_private = SigningKey.from_string(bytes.fromhex(new_owner_hex), curve=SECP256k1)
    new_owner_public = new_owner_private.verifying_key
    new_owner = new_owner_public.to_string().hex()

    plot_def_airport_intersect = PlotObjectService.create_plot(polygon_leb_airport_intersect, max_time=30, max_depth=md)
    message = plot_def_airport_intersect['squares_hash']+"_"+new_owner+"_"+str(height)
    signature = lebanon_key_private.sign(message.encode("utf-8")).hex()
    sig_tuple = [message, new_owner, lebanon_public, signature]

    
    try:
        BlockchainService.add_plot_to_tree(plot_def_airport_intersect, new_owner, sig_tuple, leaf_type="owner") 
    except BlockError as e:
        if "already assigned" in str(e):
            print("Assigned exception raised as expected.")
    
    print(555)
    #u_time = time.time()
    #BlockchainService.compute_block_tree(height=height+1)
    #u_time = time.time()-u_time

    """    
    #Assign some first owner to this plot...
    first_owner_hex = "f6b214c6e14c3a3f058976a29c8d86273b3070716601438b3978a05c29df84fd"
    first_owner_private = SigningKey.from_string(bytes.fromhex(first_owner_hex), curve=SECP256k1)
    first_owner_public = first_owner_private.verifying_key
    first_owner = first_owner_public.to_string().hex()
    """

    print("plot time: "+str(s_time))
    print("tree time: "+str(t_time))
    print("block time: "+str(u_time))

    print("Doing a transfer now:")

    doc = MongoDBLeaf.find_one({"assigned_to_owner": {"$exists": True}})
    print("doc_id: "+doc['_id'])
    second_owner_hex = "3abc18e6a7f6996fef5fd54b1e6ba90aa654edbfc3a1c586e219fcdceeedd3fd"
    second_owner_private = SigningKey.from_string(bytes.fromhex(second_owner_hex), curve=SECP256k1)
    second_owner_public = second_owner_private.verifying_key
    second_owner = second_owner_public.to_string().hex()
    
    height, _, __ = BlockchainService.get_height()

    sig_msg = ",".join([doc['_id'], new_owner, second_owner, str(height)])
    sig_sig = new_owner_private.sign(sig_msg.encode("utf-8")).hex()
    sig_tuple = (sig_msg, second_owner, new_owner, sig_sig)
    BlockchainService.transfer_leaf_to_owner(height, sig_tuple)

    BlockchainService.compute_block_tree(height=height+1)
    BlockchainService.submit_block_tree(height+1) 

    print("ownership tranfer??")
    import pdb
    pdb.set_trace()


def main2():
    pass
    #todo:
    #-create plot
    #-assign country
    #-assign owner (lots of plot)
    #-transfer owner (lots of plot)
    #-split lot.

if __name__=="__main__":
    main()
