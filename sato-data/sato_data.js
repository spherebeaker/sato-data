
function compute_block_from_pos(pos_string) {
    function subdivide_by2(square, i) {
        var tl = square['tl']
        var br = square['br']
        var w = square['w']
        var h = square['h']
        var chr = pos_string[i]
        var pos = square['pos']

        if (w == 0) {
            return;
        }
        console.log(square);
        console.log(tl);
        console.log(br);
        var mm = [Math.floor((tl[0]+br[0])/2), Math.floor((tl[1]+br[1])/2)]

        var tm = [mm[0], tl[1]]
        var tr = [br[0], tl[1]]

        var ml = [tl[0], mm[1]]
        var mr = [br[0], mm[1]]

        var bl = [tl[0], br[1]]
        var bm = [mm[0], br[1]]
      
        console.log(bm)

        if (w==h) {
            if (chr == '0') {
                return {"tl": tl, "br": bm, "w": w-1, "h": h, "pos": pos+'0'}
            }
            else {
                return {"tl": tm, "br": br, "w": w-1, "h": h, "pos": pos+'1'}
            }
        }
        else {
            if (chr == '0') {
                return {"tl": tl, "br": mr, "w": w, "h": h-1, "pos": pos+'0'}
            }
            else {
                return {"tl": ml, "br": br, "w": w, "h": h-1, "pos": pos+'1'}
            }
        }
    }
    var output = {"_id": "0_0", "tl": [0, 0],
                  "depth": 0,
                  "node_id": "",
                  "br": [Math.pow(2, 32), Math.pow(2,32)],
                  "w": 32, "h": 32, "split": false,
                  "pos": ""
                 }
    console.log(output)
    for (var i=0; i<pos_string.length;i++) {
        output = subdivide_by2(output, i);
    }
    return output
}

function getPosCoordinates(pos_string) {
    var data = compute_block_from_pos(pos_string);
    var tl = {"lsLat": data['tl'][0], "lsLon": data['tl'][1]};
    var br = {"lsLat": data['br'][0], "lsLon": data['br'][1]};
    data['tl'] = fromLotSpace(tl);
    data['br'] = fromLotSpace(br);
    return data;
}


function toLotSpace(coordinate) {
  var lat = coordinate['lat'];
  var lon = coordinate['lon'];
  lat+=90;
  lon+=180;
  var lsLat = Math.floor((lat+FLOAT_PER)*10000000);
  var lsLon = Math.floor((lon+FLOAT_PER)*10000000);
  return {"lsLat": lsLat, "lsLon": lsLon}
}

function fromLotSpace(lotCoordinate) {
  var lsLat = lotCoordinate['lsLat'];
  var lsLon = lotCoordinate['lsLon'];
  var lat = lsLat/10000000.0;
  var lon = lsLon/10000000.0;
  lat -= 90;
  lon -= 180;
  return {"lat": lat, "lon": lon}
}

function posStringToPosInt(pos_string) {
  var bin_string = "1"+pos_string;
  var number = parseInt(bin_string, 2);
  return number;
}

function posIntToPosString(number) {
  bin_number = (number >>> 0).toString(2);
  return bin_number.slice(1, bin_number.length); 
}



getPosCoordinates("001");
