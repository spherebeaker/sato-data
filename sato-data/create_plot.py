from landontoda.services.lots_service import PlotObjectService
import int_waters
import time
import json
import hashlib


def compute_block_from_pos(pos_string):
    def subdivide_by2(square, i):
        tl, br = square['tl'], square['br']
        w = square['w']
        h = square['h']
        char = pos_string[i]
        pos = square['pos']
        if w == 0:
            return None

        # tl - - tm - - tr
        # -       -     - 
        # ml     mm     mr    
        # -       -     -
        # bl     bm     br

        mm = (int((tl[0]+br[0])/2), int((tl[1]+br[1])/2))

        tl = tl
        tm = (mm[0], tl[1])
        tr = (br[0], tl[1])

        ml = (tl[0], mm[1])
        mr = (br[0], mm[1])
 
        bl = (tl[0], br[1])
        bm = (mm[0], br[1])
        br = br

        if w==h:#this is a square, so divide it into two vertical rectangles:
            if char == '0':
                return {"tl": tl, "br": bm, "w": w-1, "h": h, "pos": pos+'0'}
            else:
                return {"tl": tm, "br": br, "w": w-1, "h": h, "pos": pos+'1'}
        else:
            if char == '0':
                return {"tl": tl, "br": mr, "w": w, "h": h-1, "pos": pos+'0'}
            else:
                return {"tl": ml, "br": br, "w": w, "h": h-1, "pos": pos+'1'}
    
    output = {"_id": "0_0", "tl": [0, 0],
              "depth": 0,
              "node_id": "",
              "br": [2**32, 2**32],
              "w": 32, "h": 32, "split": False, 
              "pos": "",
              "parent": None,
              "order": None,
              "valid": False}

    for i in range(len(pos_string)):
        output = subdivide_by2(output, i)
    return output



def subdivide_by2(square):
    """
        This subdivides "square" lots into 2 sub rectangles or
        squares.  This is used for sato-servers to give a 
        binary tree.  starting with the world square, this will
        end up dividing it into two vertical rectangles.  Those
        rectangles are then divided into to squares each, which
        are divided into vertical rectangles again and so on.  
    """
    out_squares = list()
    tl, br = square['tl'], square['br']
    w = square['w']
    h = square['h']
    pos = square['pos']

    if w == 0:
        return list()

    # tl - - tm - - tr
    # -       -     - 
    # ml     mm     mr    
    # -       -     -
    # bl     bm     br

    mm = ((tl[0]+br[0])/2, (tl[1]+br[1])/2)

    tl = tl
    tm = (mm[0], tl[1])
    tr = (br[0], tl[1])

    ml = (tl[0], mm[1])
    mr = (br[0], mm[1])
 
    bl = (tl[0], br[1])
    bm = (mm[0], br[1])
    br = br

    if w==h:#this is a square, so divide it into two vertical rectangles:
        out_squares.append({"tl": tl, "br": bm, "w": w-1, "h": h, "pos": pos+'0'})
        out_squares.append({"tl": tm, "br": br, "w": w-1, "h": h, "pos": pos+'1'})
    else:
        out_squares.append({"tl": tl, "br": mr, "w": w, "h": h-1, "pos": pos+'0'})
        out_squares.append({"tl": ml, "br": br, "w": w, "h": h-1, "pos": pos+'1'})
 
    return out_squares
 

def subdivide_by4(square):
    """
        This subdivides square lots into 4 even squares.  Used
        in land32, but gives a quadtree instead of a binary tree.
        
        This should not be used for sato-servers.  This is
        here mainly for reference.
    """
    out_squares = list()
    tl, br = square['tl'], square['br']
    w = square['w']
    h = square['h']

    if w == 0:
        return list()

    mp = ((tl[0]+br[0])/2, (tl[1]+br[1])/2)
    tp = ((tl[0]+br[0])/2, tl[1])
    lp = (tl[0], (tl[1]+br[1])/2)
    rp = (br[0], (tl[1]+br[1])/2)
    bp = ((tl[0]+br[0])/2, br[1])

    ##topleft
    out_squares.append({"tl": tl, "br": mp, "w": w-1, "h": h-1 })
    ##topright
    out_squares.append({"tl": tp, "br": rp, "w": w-1, "h": h-1})
    ##bottomleft
    out_squares.append({"tl": lp, "br": bp, "w": w-1, "h": h-1})
    ##bottomright
    out_squares.append({"tl": mp, "br": br, "w": w-1, "h": h-1})
    return out_squares
                                

def create_plot_data():
    max_lat = int((180+0.0000000000001)*10000000)
    max_lon = int((360+0.0000000000001)*10000000)
    the_squares = [{"_id": "0_0", "tl": [0, 0],
                    "depth": 0,
                    "node_id": "",
                    "br": [2**32, 2**32],
                    "w": 32, "h": 32, "split": False, 
                    "pos": "",
                    "parent": None,
                    "order": None,
                    "valid": False}]

    squares_dict = {"0_0": 0}
    for depth in range(1, 14):
        depth_count = 0
        print(depth, len(the_squares))
        new_squares = list()
        for square in the_squares:
            if square['split'] == False:
                square['split'] = True
                square['children'] = list()
                sub_squares = subdivide_by2(square)
                for i, nsquare in enumerate(sub_squares):
                    nsquare['_id'] = str(depth)+"_"+str(depth_count)
                    nsquare['split'] = False
                    nsquare['parent'] = square['_id']
                    nsquare['order'] = i
                    nsquare['depth'] = depth
                    if nsquare['br'][0] >= max_lat or nsquare['br'][1] >= max_lon:
                        nsquare['valid'] = False
                    else:
                        nsquare['valid'] = True
                    
                    new_squares.append(nsquare)
                    depth_count+=1
                    square['children'].append(nsquare['_id'])

        for square in new_squares:
           squares_dict[square['_id']] = len(the_squares)
           the_squares.append(square)
    output_file = open("international_waters_plot.json", "w")
    try:
        country_data = json.loads(open("country_data.json").read())
    except:
        raise
    total = len(the_squares)

    start_time = time.time()
    for i, s in enumerate(the_squares):
        if i > 0:
            rate = i/(time.time()-start_time)
            left = (total-i)/rate
            print(i,total, i/total, rate, left)

        tl = s['tl']
        br = s['br']

        mm = ((tl[0]+br[0])/2, (tl[1]+br[1])/2)

        tl = tl
        tm = (mm[0], tl[1])
        tr = (br[0], tl[1])

        ml = (tl[0], mm[1])
        mr = (br[0], mm[1])
 
        bl = (tl[0], br[1])
        bm = (mm[0], br[1])
        br = br

        points = [tl, tm, tr, ml, mm, mr, bl, bm, br]
                 
        countries = list()
        for p in points:
            key = str(p[0])+"_"+str(p[1])
            if key not in country_data:
                country_data[key] = int_waters.get_country(p[0], p[1])
            if country_data[key]:
                countries.append(country_data[key])
 
        s['countries'] = countries
        output_file.write(json.dumps(s)+"\n")

    country_file = open("country_data.json", "w")
    country_file.write(json.dumps(country_data))
    country_file.close()
    output_file.close()
    
    def compute_hash(_id):
        def my_hash(string):
            if type(string) == str:
                string = string.encode('utf-8')
            return hashlib.sha256(string).digest()
        sq = the_squares[squares_dict[_id]]
        if "hash" in sq:
            return sq['hash']
        elif sq['depth'] == 13:
            tl = int(sq['tl'][0]), int(sq['tl'][1])
            br = int(sq['br'][0]), int(sq['br'][1])
            w = sq['w']
            h = sq['h']
            depth = sq['depth']
            pos = sq['pos']
            split = int(sq['split'])
            valid = int(sq['valid'])
            countries = ",".join(sorted(sq['countries']))
            string = "_".join([str(x) for x in [pos, tl[0], tl[1], br[0], br[1], w, h, depth, valid, countries]]).encode("utf-8")
            sq['hash_string'] = string
            sq['hash'] = my_hash(string)
            return sq['hash']
        else:
            child1 = sq['children'][0]
            child2 = sq['children'][1]
            try: 
              sq['hash_string'] = compute_hash(child1)+compute_hash(child2)
              sq['hash'] = my_hash(sq['hash_string'])
            except:
              import pdb
              pdb.set_trace()
            return sq['hash']
            

    compute_hash("0_0")
    print(len([x for x in the_squares if x['valid'] and x['split'] == False and len(x['countries']) ==0 ]))
    zz = [x for x in the_squares if x['valid'] and x['split'] == False and len(x['countries'])==0]
    fout = open("pos_out.txt", "w")
    for x in zz:
        fout.write(x['pos']+"\n")
    fout.close()
    for x in zz:
        res = compute_block_from_pos(x['pos'])
        if res['tl'][0] == x['tl'][0] and res['tl'][1] == x['tl'][1] and\
           res['br'][0] == x['br'][0] and res['br'][1] == x['br'][1]:
            continue
        print("fail")
    ##export the squares:
    export = list()
    for sq in the_squares:
        sq['tl'] = int(sq['tl'][0]), int(sq['tl'][1])
        sq['br'] = int(sq['br'][0]), int(sq['br'][1])

        sq['hash_string'] = sq['hash_string'].hex()
        sq['hash'] = sq['hash'].hex()
        export.append(sq)
    data = {"squares": export, "version": "1.0"}
    output_files = open("tree_data.json", "w").write(json.dumps(data))
    import pdb
    pdb.set_trace()

def main():
    create_plot_data()

if __name__=='__main__':
    main()    
    

