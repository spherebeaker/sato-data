import http.client
import json
import pprint
import time
from landontoda.services.lots_service import LOTSpace


def get_data(lat, lon):
    url = "https://nominatim.openstreetmap.org/reverse?lat=<value>&lon=<value>&<params>"
    headers = {"User-Agent": "SatoServer"}
    conn = http.client.HTTPSConnection("nominatim.openstreetmap.org", 443)
    conn.request("GET", "/reverse?lat="+str(lat)+"&lon="+str(lon)+"&format=json", headers=headers)
    response = conn.getresponse()
    data = response.read().decode('utf-8')
    conn.close()

    doc = json.loads(data)
    #pprint.pprint(doc)
    time.sleep(0.5)
    return doc

def get_country(ilat, ilon):
    coord = LOTSpace.from_lot_space({"ls_lat": ilat, "ls_lon": ilon})
    lat, lon = coord['lat'], coord['lon']
  
    try:
        doc = get_data(lat, lon)
        country = doc.get("address",{}).get('country_code')
        return country
    except:
        return None


def main():
    lon = -124.6294442+180
    lat = 37.2146322

    lat, lon = world_to_space(lat, lon)

    print(lat,lon)
    doc = get_country(lat, lon)
    print(doc)
    lat, lon = (20.20, 40.40)

    lat, lon = world_to_space(lat, lon)

    doc = get_country(lat, lon)
    print(doc)

    lon = -124.6294442
    lat = 37.2146322

    lat, lon = world_to_space(lat, lon)

    print(lat,lon)
    doc = get_country(lat, lon)
    print(doc)
 

if __name__=='__main__':
    main()
